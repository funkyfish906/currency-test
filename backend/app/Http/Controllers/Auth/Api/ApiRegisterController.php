<?php

namespace App\Http\Controllers\Auth\Api;


use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Http\Request;

class ApiRegisterController extends RegisterController
{

    public function register(Request $request)
    {

        $data = request()->only('email', 'name', 'password','password_confirmation');

        $validator = $this->validator($data);

        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), 400);
        }

        $user = $this->create($data);

        $request->request->add([
            'grant_type'    => 'password',
            'client_id'     => $request->client_id,
            'client_secret' => $request->client_secret,
            'username'      => $data['email'],
            'password'      => $data['password'],
            'scope'         => null,
        ]);

        $proxy = Request::create(
            'oauth/token',
            'POST'
        );

        return \Route::dispatch($proxy);
    }

}