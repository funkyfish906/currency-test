<?php

namespace App\Http\Controllers\Auth\Api;

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiLoginController extends LoginController
{

    public function logout(Request $request)
    {

        $request->user()->token()->revoke();


        $json = [
            'success' => true,
            'code' => 200,
            'message' => 'You are Logged out.',
        ];
        
        return response()->json($json, '200');

    }



}