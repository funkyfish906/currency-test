import Vue from 'vue'
import App from './App.vue'
import Router from './routse'
import VueResource from 'vue-resource'
import Auth from './packages/auth/Auth'
import VeeValidate from 'vee-validate';
import store from './store/store';


Vue.use(VueResource);
Vue.use(Auth);
Vue.use(VeeValidate);
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.http.options.root = ('http://127.0.0.1:8000');

Vue.http.interceptors.push(function(request, next) {
    request.headers.set('Authorization', 'Bearer ' + Vue.auth.getToken());
    next();
});

Vue.http.interceptors.push( ( request, next) =>{
    next(response =>{
        if (response.status == 404 ){
            swal(response.status.toString(), response.body.error, 'error')
        } else if (response.status == 500){
            swal(response.status.toString(), 'Problem in our server', 'error')
        }
    })
});

Router.beforeEach(
    (to, from, next) => {

        if(to.matched.some(record => record.meta.forVisitors)){
            if( Vue.auth.isAuthenticated()){
                next({
                    path: '/feed'
                })
            } else next()
        }
        else if(to.matched.some(record => record.meta.forAuth)){
            console.log();
            if( ! Vue.auth.isAuthenticated()){
                next({
                    path: '/login'
                })
            } else next()
        }

        else next()
    }
);
new Vue({
    el: '#app',
    router: Router,
    store,
    render: h => h(App)

});
